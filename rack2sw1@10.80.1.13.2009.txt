
version 12.1
no service pad
service timestamps debug uptime
service timestamps log uptime
no service password-encryption
!
hostname rack2sw1
!
!
ip subnet-zero
!
ip ssh time-out 120
ip ssh authentication-retries 3
vtp domain CIT345PROJ
vtp mode transparent
!
spanning-tree mode pvst
no spanning-tree optimize bpdu transmission
spanning-tree extend system-id
!
!
!
!
vlan 101 
!
vlan 201
 name Store1_VLAN
!
vlan 202
 name Store10_VLAN
!
vlan 203
 name Store12_VLAN
!
vlan 204
 name Store8_VLAN
!
vlan 205
 name Store11_VLAN
!
vlan 206
 name Store4_VLAN
!
vlan 207
 name Store15_VLAN
!
vlan 208
 name Store6_VLAN
!
vlan 209
 name Store2_VLAN
!
vlan 210
 name Store24_VLAN
!
vlan 211
 name Store17_VLAN
!
vlan 212
 name Store18_VLAN
!
vlan 213
 name Store13_VLAN
!
vlan 214
 name Store103_VLAN
!
vlan 215
 name Store202_VLAN
!
vlan 216
 name Store21_VLAN
!
vlan 217
 name Store23_VLAN
!
vlan 218
 name Store101_VLAN
!
vlan 219
 name Store102_VLAN
!
vlan 220
 name Store201_VLAN
!
vlan 221
 name HQDMZ_VLAN
!
vlan 2202-2203 
!
interface FastEthernet0/1
 switchport access vlan 2202
 switchport mode trunk
!
interface FastEthernet0/2
 switchport access vlan 202
!
interface FastEthernet0/3
 switchport access vlan 203
!
interface FastEthernet0/4
 switchport access vlan 204
!
interface FastEthernet0/5
 switchport access vlan 205
!
interface FastEthernet0/6
 switchport access vlan 206
!
interface FastEthernet0/7
 switchport access vlan 207
!
interface FastEthernet0/8
 switchport access vlan 208
!
interface FastEthernet0/9
 switchport access vlan 209
!
interface FastEthernet0/10
 switchport access vlan 210
!
interface FastEthernet0/11
 switchport access vlan 211
!
interface FastEthernet0/12
 switchport access vlan 212
!
interface FastEthernet0/13
 switchport access vlan 213
!
interface FastEthernet0/14
 switchport access vlan 214
 switchport mode trunk
!
interface FastEthernet0/15
 switchport access vlan 215
!
interface FastEthernet0/16
 switchport access vlan 216
!
interface FastEthernet0/17
 switchport access vlan 217
!
interface FastEthernet0/18
 switchport access vlan 218
!
interface FastEthernet0/19
 switchport access vlan 219
!
interface FastEthernet0/20
 switchport access vlan 220
!
interface FastEthernet0/21
 switchport access vlan 2203
 switchport mode trunk
!
interface FastEthernet0/22
!
interface FastEthernet0/23
!
interface FastEthernet0/24
!
interface FastEthernet0/25
!
interface FastEthernet0/26
!
interface FastEthernet0/27
!
interface FastEthernet0/28
!
interface FastEthernet0/29
 switchport access vlan 201
 switchport mode access
!
interface FastEthernet0/30
!
interface FastEthernet0/31
!
interface FastEthernet0/32
!
interface FastEthernet0/33
 switchport access vlan 101
 spanning-tree portfast
!
interface FastEthernet0/34
!
interface FastEthernet0/35
 switchport access vlan 101
!
interface FastEthernet0/36
!
interface FastEthernet0/37
!
interface FastEthernet0/38
!
interface FastEthernet0/39
!
interface FastEthernet0/40
!
interface FastEthernet0/41
!
interface FastEthernet0/42
!
interface FastEthernet0/43
!
interface FastEthernet0/44
!
interface FastEthernet0/45
!
interface FastEthernet0/46
!
interface FastEthernet0/47
!
interface FastEthernet0/48
 switchport mode trunk
!
interface GigabitEthernet0/1
!
interface GigabitEthernet0/2
!
interface Vlan1
 no ip address
 no ip route-cache
 shutdown
!
interface Vlan2202
 ip address 10.22.2.200 255.255.255.0
 no ip route-cache
!
interface Vlan2203
 ip address 10.22.3.200 255.255.255.0
 no ip route-cache
 shutdown
!
ip http server
!
line con 0
line vty 0 4
 login
line vty 5 15
 login
!
!
end
