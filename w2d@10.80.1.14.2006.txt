
!
! ADTRAN, Inc. OS version A1.04.00.E
! Boot ROM version 14.04.00
! Platform: Total Access 912 (2nd Gen), part number 4212912L1
! Serial number 912-121B_001
!
!
hostname "w2d"
no enable password
!
!
ip subnet-zero
ip classless
ip routing
!
!
!
!
no auto-config
!
event-history on
no logging forwarding
no logging email
!
no service password-encryption
!
banner exec %
___________                     __________       .__  .__   
\__    ___/____    ____  ____   \______   \ ____ |  | |  |  
  |    |  \__  \  / ___\/  _ \   |    |  _// __ \|  | |  |  
  |    |   / __ \| \__ (  <_> )  |    |   \  ___/|  |_|  |__
  |____|  (____  /\___  >____/   |______  /\___  >____/____/
               \/     \/                \/     \/           
%
!
!
no ip firewall alg msn
no ip firewall alg h323
!
!
!
!
!
no dot11ap access-point-control
!
!
!
!
!
!
!
!
!
!
!
!
interface loop 1
  ip address  6.6.6.6  255.255.255.255 
  no shutdown
!
interface eth 0/1
  ip address  10.4.8.65  255.255.255.192 
  no shutdown
!
!
!
!
interface t1 0/1
  tdm-group 1 timeslots 1 speed 64
  no shutdown
!
interface t1 0/2
  no shutdown
!
!
interface fxs 0/1
  no shutdown
!
interface fxs 0/2
  no shutdown
!
interface fxs 0/3
  no shutdown
!
interface fxs 0/4
  no shutdown
!
interface fxs 0/5
  no shutdown
!
interface fxs 0/6
  no shutdown
!
interface fxs 0/7
  no shutdown
!
interface fxs 0/8
  no shutdown
!
interface fxs 0/9
  no shutdown
!
interface fxs 0/10
  no shutdown
!
interface fxs 0/11
  no shutdown
!
interface fxs 0/12
  no shutdown
!
interface ppp 1
  ip address  10.4.8.2  255.255.255.252 
  no shutdown
  cross-connect 1 t1 0/1 1 ppp 1
!
!
!
!
router ospf
  network 10.4.0.0 0.0.255.255 area 4
  network 10.0.0.0 0.0.255.255 area 4
!
!
!
!
!
no ip tftp server
no ip tftp server overwrite
no ip http server
no ip http secure-server
no ip snmp agent
no ip ftp server
no ip scp server
no ip sntp server
!
!
!
!
!
!
!
!
voice feature-mode network
voice forward-mode network
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
ip sip
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
line con 0
  no login
!
line telnet 0 4
  login
  no shutdown
line ssh 0 4
  login local-userlist
  no shutdown
!
!
end
