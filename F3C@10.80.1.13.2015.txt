
version 12.2
service timestamps debug uptime
service timestamps log uptime
no service password-encryption
!
hostname F3C
!
!
ip subnet-zero
!
!
!
partition flash 2 8 8
!
!
!
!
interface Ethernet0/0
 ip address 10.3.36.1 255.255.255.192
 half-duplex
!
interface Ethernet0/0.1
!
interface Ethernet0/0.2
!
interface Serial0/0
 ip address 10.3.39.2 255.255.255.252
 ip route-cache flow
!
router ospf 1
 log-adjacency-changes
 network 0.0.0.0 255.255.255.255 area 3
!
ip flow-export destination 10.0.0.10 2055
ip classless
no ip http server
!
snmp-server community barlow RO
snmp-server community hansen RW
snmp-server enable traps tty
banner exec ^C
 /$$      /$$                    /$$  /$$$$$$ 
| $$  /$ | $$                   /$$/ /$$__  $$
| $$ /$$$| $$  /$$$$$$         /$$/ |__/  \ $$
| $$/$$ $$ $$ /$$__  $$       /$$/     /$$$$$/
| $$$$_  $$$$| $$$$$$$$      |  $$    |___  $$
| $$$/ \  $$$| $$_____/       \  $$  /$$  \ $$
| $$/   \  $$|  $$$$$$$        \  $$|  $$$$$$/
|__/     \__/ \_______/         \__/ \______/ 
                                              
      /$$$$$           /$$                    
      |__  $$          | $$                   
         | $$  /$$$$$$ | $$$$$$$  /$$$$$$$    
         | $$ /$$__  $$| $$__  $$| $$__  $$   
    /$$  | $$| $$  \ $$| $$  \ $$| $$  \ $$   
   | $$  | $$| $$  | $$| $$  | $$| $$  | $$   
   |  $$$$$$/|  $$$$$$/| $$  | $$| $$  | $$   
    \______/  \______/ |__/  |__/|__/  |__/   
^C
!
line con 0
line aux 0
line vty 0 4
 login
!
end
