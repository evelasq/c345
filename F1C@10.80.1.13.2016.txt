
version 12.3
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname F1C
!
boot-start-marker
boot-end-marker
!
!
no aaa new-model
ip subnet-zero
!
!
!
ip cef
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
interface Ethernet0/0
 no ip address
 half-duplex
!
interface Ethernet0/0.1
 encapsulation dot1Q 201
 ip address 10.3.28.1 255.255.255.192
!
interface Ethernet0/0.2
 encapsulation dot1Q 202
 ip address 10.3.28.65 255.255.255.192
!
interface Serial0/0
 ip address 10.3.29.2 255.255.255.252
 ip route-cache flow
 no ip mroute-cache
 no fair-queue
!
router ospf 2
 log-adjacency-changes
 network 10.0.0.112 0.0.0.3 area 2
!
router ospf 1
 log-adjacency-changes
 network 10.0.0.0 0.0.0.3 area 3
 network 0.0.0.0 255.255.255.255 area 3
!
no ip http server
ip classless
ip route 0.0.0.0 0.0.0.0 Ethernet0/0
ip flow-export version 9
ip flow-export destination 10.0.0.10 2055
!
!
!
!
snmp-server community barlow RO
snmp-server community hansen RW
!
!
!
banner exec ^C
 /$$      /$$                    /$$  /$$$$$$ 
| $$  /$ | $$                   /$$/ /$$__  $$
| $$ /$$$| $$  /$$$$$$         /$$/ |__/  \ $$
| $$/$$ $$ $$ /$$__  $$       /$$/     /$$$$$/
| $$$$_  $$$$| $$$$$$$$      |  $$    |___  $$
| $$$/ \  $$$| $$_____/       \  $$  /$$  \ $$
| $$/   \  $$|  $$$$$$$        \  $$|  $$$$$$/
|__/     \__/ \_______/         \__/ \______/ 
                                              
      /$$$$$           /$$                    
      |__  $$          | $$                   
         | $$  /$$$$$$ | $$$$$$$  /$$$$$$$    
         | $$ /$$__  $$| $$__  $$| $$__  $$   
    /$$  | $$| $$  \ $$| $$  \ $$| $$  \ $$   
   | $$  | $$| $$  | $$| $$  | $$| $$  | $$   
   |  $$$$$$/|  $$$$$$/| $$  | $$| $$  | $$   
    \______/  \______/ |__/  |__/|__/  |__/   
^C
!
line con 0
line aux 0
line vty 0 4
 login
!
!
end
