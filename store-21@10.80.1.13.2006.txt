
version 12.3
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname store-21
!
boot-start-marker
boot-end-marker
!
!
memory-size iomem 15
no aaa new-model
ip subnet-zero
ip cef
!
!
no ip domain lookup
!
ip audit po max-events 100
!
!
!
!
!
interface Ethernet0/0
 ip address 10.3.136.1 255.255.255.192
 ip access-group 101 in
 ip access-group 101 out
 half-duplex
!
interface Ethernet0/0.101
 description pos
!
interface Ethernet0/0.102
 description voice
!
interface Ethernet0/0.103
 description wireless-production
!
interface Ethernet0/0.104
 description wireless-voice
!
interface Ethernet0/0.105
 description starbucks
!
interface Ethernet0/0.106
 description pharmacy
 ip access-group BLOCKINTERVLAN in
!
interface Ethernet0/0.107
 description scales
!
interface Ethernet0/0.108
 description office
!
interface Ethernet0/0.109
 description production
!
interface Serial0/0
 no ip address
 ip route-cache flow
 shutdown
 clock rate 9600
!
interface Ethernet0/1
 no ip address
 shutdown
 half-duplex
!
interface Serial0/1
 ip address 10.3.139.2 255.255.255.252
 ip access-group BLOCKNONPHARMACY in
 ip route-cache flow
!
interface Serial1/0
 no ip address
 shutdown
!
interface Serial1/1
 no ip address
 ip access-group BLOCKNONPHARMACY in
 ip route-cache flow
 clock rate 64000
!
interface Serial1/2
 no ip address
 shutdown
!
interface Serial1/3
 no ip address
 shutdown
!
router ospf 100
 log-adjacency-changes
 network 0.0.0.0 255.255.255.255 area 3
!
no ip http server
ip flow-export version 9
ip flow-export destination 10.0.0.10 2055
ip classless
!
!
!
ip access-list extended BLOCKINTERVLAN
 permit ip 172.16.0.0 0.0.255.255 172.16.0.0 0.0.255.255
ip access-list extended BLOCKNONPHARMACY
 permit ospf any any
 permit ip 172.16.0.0 0.0.255.255 172.16.0.0 0.0.255.255
 permit ip 10.0.0.0 0.255.255.255 10.0.0.0 0.255.255.255
access-list 101 deny   ip any 172.16.21.0 0.0.0.255
access-list 101 permit ip 0.0.0.0 172.16.21.0 0.0.0.0 0.0.0.255
snmp-server community hansen RW
snmp-server community barlow RO
banner exec ^C
 /$$      /$$                    /$$  /$$$$$$ 
| $$  /$ | $$                   /$$/ /$$__  $$
| $$ /$$$| $$  /$$$$$$         /$$/ |__/  \ $$
| $$/$$ $$ $$ /$$__  $$       /$$/     /$$$$$/
| $$$$_  $$$$| $$$$$$$$      |  $$    |___  $$
| $$$/ \  $$$| $$_____/       \  $$  /$$  \ $$
| $$/   \  $$|  $$$$$$$        \  $$|  $$$$$$/
|__/     \__/ \_______/         \__/ \______/ 
                                              
      /$$$$$           /$$                    
      |__  $$          | $$                   
         | $$  /$$$$$$ | $$$$$$$  /$$$$$$$    
         | $$ /$$__  $$| $$__  $$| $$__  $$   
    /$$  | $$| $$  \ $$| $$  \ $$| $$  \ $$   
   | $$  | $$| $$  | $$| $$  | $$| $$  | $$   
   |  $$$$$$/|  $$$$$$/| $$  | $$| $$  | $$   
    \______/  \______/ |__/  |__/|__/  |__/   
^C
!
line con 0
 exec-timeout 0 0
 logging synchronous
line aux 0
line vty 0 4
 login
!
!
end
